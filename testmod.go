package testmod

import "math"

func Test(s1 string, s2 string)string{
	return s1 + s2
}

func Test1(a float64, b float64, c float64)(float64, float64){
	res1 := -b + math.Sqrt(b * b - 4.0 * a * c)/(2*a)
	res2 := -b - math.Sqrt(math.Pow(b, 2) - 4 * a * c)/(2*a)
	return res1, res2
}

func Test2(a int, b int)(int, int){
	res2 := b >> 3
	res1 := a << 2
	return res1, res2
}
